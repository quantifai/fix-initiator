# fix-initiator

Basic acceptor initiator interface to test FIX application level changes

## Dependencies

- Just install quickfix: `pip3 install quickfix`

## Running

- In one terminal:

```bash
python3 acceptor.py
```

- In the other terminal:

```bash
python3 initiator.py initiatorsettings.cfg
```

Just choose option one to simulate a trade

